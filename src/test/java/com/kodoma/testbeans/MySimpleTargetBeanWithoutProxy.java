package com.kodoma.testbeans;

/**
 * Класс для тестирования прокси.
 * Created on 03.06.2018.
 * @author Kodoma.
 */
public class MySimpleTargetBeanWithoutProxy implements MySimpleBeanI {

    @Override
    public void go() {
    }

    @Override
    public void foo() {
    }

    @Override
    public void bar() {
    }
}
