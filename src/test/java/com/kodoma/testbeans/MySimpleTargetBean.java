package com.kodoma.testbeans;

import com.kodoma.proxy.annotations.LogMethodCall;

public class MySimpleTargetBean implements MySimpleBeanI {

    @LogMethodCall
    @Override
    public void go() {
    }

    @Override
    public void foo() {
    }

    @Override
    public void bar() {
    }
}

