package com.kodoma.testbeans;

public interface MySimpleBeanI {

    void go();

    void foo();

    void bar();
}
