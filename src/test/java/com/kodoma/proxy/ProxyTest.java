package com.kodoma.proxy;

import com.kodoma.proxy.testhandler.ProxyHandler;
import com.kodoma.testbeans.MySimpleBeanI;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Тест для {@link ProxyHandler}
 * Created on 03.06.2018.
 * @author Kodoma.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:proxy-test-context.xml"})
public class ProxyTest extends AbstractJUnit4SpringContextTests {

    @Autowired
    private MySimpleBeanI mySimpleTargetBean;

    @Autowired
    private MySimpleBeanI mySimpleTargetBeanWithoutProxy;

    @Test
    public void test() {
        mySimpleTargetBean.go();
        //go(mySimpleTargetBeanWithoutProxy);
        //go(mySimpleTargetBean);
    }

    private static void go(final MySimpleBeanI bean) {
        final long start = System.currentTimeMillis();
        for (int i = 0; i < 1000000; i++) {
            bean.foo();
            bean.bar();
            bean.go();
        }
        System.out.println("Заняло времени: " + (System.currentTimeMillis() - start));
    }
}
