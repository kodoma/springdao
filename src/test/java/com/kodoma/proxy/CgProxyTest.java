package com.kodoma.proxy;

import com.kodoma.testbeans.MySimpleBeanI;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created on 03.06.2018.
 * @author Kodoma.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:proxy-test-context.xml"})
public class CgProxyTest extends AbstractJUnit4SpringContextTests {

    @Autowired
    @Qualifier("mySimpleTargetBean")
    private MySimpleBeanI mySimpleTargetBean;

    @Autowired
    @Qualifier("mySimpleTargetBeanWithoutProxy")
    private MySimpleBeanI mySimpleTargetBeanWithoutProxy;

    @Test
    public void test() {
        mySimpleTargetBean.go();
        mySimpleTargetBean.foo();
        mySimpleTargetBean.bar();
/*        go(mySimpleTargetBeanWithoutProxy);
        go(mySimpleTargetBean);*/
    }

    private static void go(final MySimpleBeanI bean) {
        final long start = System.currentTimeMillis();
        for (int i = 0; i < 10000000; i++) {
            bean.foo();
            bean.bar();
            bean.go();
        }
        System.out.println("Заняло времени: " + (System.currentTimeMillis() - start));
    }
}
