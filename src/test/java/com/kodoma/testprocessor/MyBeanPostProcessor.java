package com.kodoma.testprocessor;

import com.kodoma.proxy.annotations.LogMethodCall;
import com.kodoma.proxy.testhandler.ProxyHandler;
import org.apache.commons.lang3.reflect.MethodUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

import java.lang.reflect.Method;
import java.util.List;

/**
 * Позволяет обрабатывать дейстивия до и после создания любого бина.
 * Обработка контейнером.
 */
public class MyBeanPostProcessor implements BeanPostProcessor {

    /** Обработчик прокси-объектов. */
    private ProxyHandler proxyHandler;

    @Override
    public Object postProcessBeforeInitialization(Object bean, String s) throws BeansException {
        final Class<?> beanClass = bean.getClass();
        final List<Method> methodsWithAnnotation = MethodUtils.getMethodsListWithAnnotation(beanClass, LogMethodCall.class);

/*        if (!methodsWithAnnotation.isEmpty()) {
            ReflectionUtils.doWithMethods(beanClass, method -> {
                if (methodsWithAnnotation.contains(method)) {
                    ReflectionUtils.makeAccessible(method);
                    ReflectionUtils.invokeMethod(method, proxyHandler.getProxy(bean));
                }
            });

        }*/
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object o, String s) throws BeansException {
        return o;
    }

    /**
     * Устанавливает {@link #proxyHandler}
     * @param proxyHandler значение для {@link #proxyHandler}.
     */
    public void setProxyHandler(ProxyHandler proxyHandler) {
        this.proxyHandler = proxyHandler;
    }
}
