package com.kodoma;

import com.kodoma.dao.Mp3DAO;
import com.kodoma.data.MP3;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Arrays;
import java.util.List;

public class Start {
    public static void main(String[] args) {
        final ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("context.xml");
        final Mp3DAO mp3DAO = (Mp3DAO)context.getBean("mySqlMp3DAO");

        final List<MP3> mp3List = Arrays.asList(
                new MP3("Origa", "Aurora"),
                new MP3("Witcher", "Ciri"),
                new MP3("Two Steps From Hell", "Protectors of the Earth")
        );

        mp3DAO.addMP3List(mp3List);
        mp3DAO.addMP3List(mp3List);
        mp3DAO.deleteById(5);
    }
}
