package com.kodoma.dao;

import com.kodoma.constants.Queries;
import com.kodoma.data.MP3;
import com.kodoma.proxy.annotations.LogMethodCall;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class MySqlMp3DAO implements Mp3DAO {

    private JdbcTemplate template;

    @Override
    public void insert(MP3 mp3) {
        template.update(Queries.INSERT, mp3.getName(), mp3.getAuthor());
    }

    @LogMethodCall
    @Override
    public void addMP3List(List<MP3> mp3List) {
        template.batchUpdate(Queries.INSERT, new BatchPreparedStatementSetter() {

            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                final MP3 mp3 = mp3List.get(i);

                ps.setString(1, mp3.getName());
                ps.setString(2, mp3.getAuthor());
            }

            @Override
            public int getBatchSize() {
                return mp3List.size();
            }
        });
    }

    public void delete(MP3 mp3) {
        // TODO add code
    }

    @Override
    public void deleteById(int id) {
        template.update(Queries.DELETE_BY_ID, id);
    }

    public MP3 getMP3Byid(int id) {
        // TODO add code
        return null;
    }

    public List<MP3> getMP3ListByName(String name) {
        // TODO add code
        return null;
    }

    public List<MP3> getMP3ListByAuthor(String name) {
        // TODO add code
        return null;
    }

    @Required
    public void setDataSource(DataSource dataSource) {
        this.template = new JdbcTemplate(dataSource);
    }
}
