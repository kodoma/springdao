package com.kodoma.dao;

import com.kodoma.constants.Queries;
import com.kodoma.data.MP3;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

/**
 * Простенькая реализация DAO через Connection.
 */
public class BaseJdbcMp3DAO implements Mp3DAO {

    public void insert(MP3 mp3) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            final String url = "jdbc:mysql://localhost:3306/music_data?serverTimezone=UTC";
            final Connection connection = DriverManager.getConnection(url, "root", "root");
            final PreparedStatement ps = connection.prepareStatement(Queries.INSERT);

            ps.setString(1, "Queen");
            ps.setString(2, "Rock");

            ps.execute();

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addMP3List(List<MP3> mp3List) {
    }

    public void delete(MP3 mp3) {
    }

    @Override
    public void deleteById(int id) {

    }

    public MP3 getMP3Byid(int id) {
        return null;
    }

    public List<MP3> getMP3ListByName(String name) {
        return null;
    }

    public List<MP3> getMP3ListByAuthor(String name) {
        return null;
    }

    @Override
    public void setDataSource(DataSource dataSource) {
    }
}
