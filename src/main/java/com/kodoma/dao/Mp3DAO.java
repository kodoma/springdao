package com.kodoma.dao;

import com.kodoma.data.MP3;

import javax.sql.DataSource;
import java.util.List;

public interface Mp3DAO {

    void insert(MP3 mp3);

    void addMP3List(List<MP3> mp3List);

    void delete(MP3 mp3);

    void deleteById(int id);

    MP3 getMP3Byid(int id);

    List<MP3> getMP3ListByName(String name);

    List<MP3> getMP3ListByAuthor(String name);

    void setDataSource(DataSource dataSource);
}
