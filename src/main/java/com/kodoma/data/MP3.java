package com.kodoma.data;

public class MP3 {

    /** Название композиции. */
    private String name;

    /** Имя автора копмозиции. */
    private String author;

    public MP3() {
    }

    public MP3(String name, String author) {
        this.name = name;
        this.author = author;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
}
