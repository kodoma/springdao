package com.kodoma;

import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jdt.core.dom.*;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Тест для ASTVisitor.
 * Created on 15.06.2018.
 * @author Kodoma.
 */
public class VisitorStart {

    public static void main(String[] args) {

        String source = null;
        try {
            source = readFile("C:\\Users\\Упя\\IdeaProjects\\springdao\\src\\main\\java\\com\\kodoma\\Test.java");
        } catch (IOException e) {
            e.printStackTrace();
        }

        final ASTParser parser = ASTParser.newParser(AST.JLS8);
        parser.setSource(source.toCharArray());
        parser.setKind(ASTParser.K_COMPILATION_UNIT);

        final CompilationUnit unit = (CompilationUnit)parser.createAST(new NullProgressMonitor());

        unit.accept(new ASTVisitor() {
            private final List<MethodDeclaration> methods = new ArrayList<>();

            public List<MethodDeclaration> perform(ASTNode node) {
                node.accept(this);
                return this.getMethods();
            }

            @Override
            public boolean visit (final MethodDeclaration method) {
                methods.add(method);
                final List statements = method.getBody().statements();

                for (Object statement : statements) {
                    final List modifiers = ((VariableDeclarationStatement)statement).modifiers();
                    for (Object modifier : modifiers) {
                        if (modifier instanceof MarkerAnnotation) {
                            System.out.println("Found annotation " + modifiers + " in method " + method.getName());
                        }
                    }
                }
                return super.visit(method);
            }

            /**
             * @return an immutable list view of the methods discovered by this visitor
             */
            public List <MethodDeclaration> getMethods() {
                return Collections.unmodifiableList(methods);
            }
        });
    }

    private static String readFile(String file) throws IOException {
        final StringBuilder stringBuilder = new StringBuilder();
        final String ls = System.getProperty("line.separator");
        String line;

        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line).append(ls);
            }
            return stringBuilder.toString();
        }
    }
}
