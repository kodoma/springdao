package com.kodoma.constants;

public class Queries {

    public static final String INSERT = "INSERT INTO mp3 (name, author) VALUES (?, ?)";

    public static final String DELETE_BY_ID = "DELETE FROM mp3 WHERE id = ?;";
}
