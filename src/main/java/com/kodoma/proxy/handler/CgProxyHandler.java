package com.kodoma.proxy.handler;

import org.springframework.cglib.proxy.Enhancer;
import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Method;
import java.util.Set;

/**
 * Прокси-handler на основе библиотеки cglib.
 * Created on 03.06.2018.
 * @author Kodoma.
 */
public class CgProxyHandler implements MethodInterceptor {

    /** Кэш аннотированных методов. */
    private Set<Method> methods;

    @Override
    public Object intercept(Object o, Method method, Object[] args, MethodProxy methodProxy) throws Throwable {
        if (methods.contains(method)) {
            System.out.println("CgProxy : " + method.getName());
        }
        return methodProxy.invokeSuper(o, args);
    }

    public Object getProxy(final Object bean, final Set<Method> methods) {
        this.methods = methods;

        final Enhancer enhancer = new Enhancer();
        final Class<?> beanClass = bean.getClass();

        enhancer.setCallback(this);
        enhancer.setSuperclass(beanClass);
        enhancer.setInterfaces(beanClass.getInterfaces());
        enhancer.setClassLoader(beanClass.getClassLoader());

        final Object proxy = beanClass.cast(enhancer.create());
        ReflectionUtils.shallowCopyFieldState(bean, proxy);

        return proxy;
    }
}
