package com.kodoma.proxy.handler;

import com.kodoma.proxy.annotations.LogMethodCall;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class ProxyHandler implements InvocationHandler {

    /** Проксируемый объект. */
    private Object proxied;

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        final Method m = proxied.getClass().getMethod(method.getName(), method.getParameterTypes());

        if (m.isAnnotationPresent(LogMethodCall.class)) {
            System.out.println("Call " + proxied.getClass().getSimpleName() + ": " + m.getName());
        }
        return method.invoke(proxied, args);
    }

    public Object getProxy(final Object proxied) {
        this.proxied = proxied;
        return Proxy.newProxyInstance(ProxyHandler.class.getClassLoader(), proxied.getClass().getInterfaces(), this);
    }
}

