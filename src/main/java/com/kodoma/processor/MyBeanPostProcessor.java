package com.kodoma.processor;

import com.kodoma.proxy.annotations.LogMethodCall;
import com.kodoma.proxy.handler.ProxyHandler;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

import java.lang.reflect.Method;

/**
 * Позволяет обрабатывать дейстивия до и после создания любого бина.
 * Обработка контейнером.
 */
public class MyBeanPostProcessor implements BeanPostProcessor {

    /** Обработчик прокси-объектов. */
    private ProxyHandler proxyHandler;

    @Override
    public Object postProcessBeforeInitialization(Object o, String s) throws BeansException {
        final Class<?> clazz = o.getClass();
        boolean found = false;
        for (Method method : clazz.getMethods()) {
            if (method.isAnnotationPresent(LogMethodCall.class)) {
                found = true;
                break;
            }
        }
        if (found) {
            System.out.println(clazz.getSimpleName() + " contains LogMethodCall annotation, try to proxied...");
            return proxyHandler.getProxy(o);
        }
        return o;
    }

    @Override
    public Object postProcessAfterInitialization(Object o, String s) throws BeansException {
        return o;
    }

    /**
     * Устанавливает {@link #proxyHandler}
     * @param proxyHandler значение для {@link #proxyHandler}.
     */
    public void setProxyHandler(ProxyHandler proxyHandler) {
        this.proxyHandler = proxyHandler;
    }
}
