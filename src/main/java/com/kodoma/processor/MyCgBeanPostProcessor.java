package com.kodoma.processor;

import com.google.common.collect.Sets;
import com.kodoma.proxy.annotations.LogMethodCall;
import com.kodoma.proxy.handler.CgProxyHandler;
import org.apache.commons.lang3.reflect.MethodUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

import java.lang.reflect.Method;
import java.util.List;

/**
 * Построцессор с {@link CgProxyHandler}.
 * Created on 03.06.2018.
 * @author йц.
 */
public class MyCgBeanPostProcessor implements BeanPostProcessor {

    /** Обработчик прокси-объектов. */
    private CgProxyHandler cgProxyHandler;

    @Override
    public Object postProcessBeforeInitialization(Object o, String s) throws BeansException {
        return o;
    }

    @Override
    public Object postProcessAfterInitialization(final Object bean, String s) throws BeansException {
        final List<Method> annotatedMethods = MethodUtils.getMethodsListWithAnnotation(bean.getClass(), LogMethodCall.class);

        if (!annotatedMethods.isEmpty()) {
            return cgProxyHandler.getProxy(bean, Sets.newHashSet(annotatedMethods));
        }
        return bean;
    }

    /**
     * Устанавливает {@link #cgProxyHandler}
     * @param cgProxyHandler значение для {@link #cgProxyHandler}.
     */
    public void setCgProxyHandler(CgProxyHandler cgProxyHandler) {
        this.cgProxyHandler = cgProxyHandler;
    }
}
